# Teste para vaga de Desenvolvedor Wordpress (Diários Associados)

O intuito deste teste é avaliar seu conhecimento no desenvolvimento de um tema para Wordpress.

Os arquivos necessários podem ser visualizados por esta plataforma em Source (Origem), é possível visualizar um preview através da imagem "layout-home.png".

### **Como deve ser a entrega:**

- Crie um repositório privado no Bitbucket: https://bitbucket.org/
- Descrever no README.md do seu projeto o usuário e senha de administrador utilizado na instalação.

### **O que esperamos na entrega:**

1. Instalação da última versão do wordpress.

2. Criar um tema com base nos arquivos disponibilizados, arquivos padrões de CSS, javaScript e imagens, podem ser encontrados na pasta assets.

3. A página inicial deve ser representada pelo arquivo index.html.

4. Na página inicial, últimas notícias devem representar os posts.

5. Na página inicial, projetos devem ser representados por um post personalizado.

6. Últimas notícias deve possuir uma página interna baseada no HTML internal.html.

7. Os itens projetos devem possuir uma página interna baseada no HTML internal-of-projects.html.

8. Será avaliado a qualidade do seu código e organização dos arquivos.

### **Ao final:**

- Versionar apenas "wp-content", "wp-config.php" e Banco de dados (.sql, .dump).
- Adicione os usuários brunosanches e leotups como ADMIN, por favor :).
- Envie um email para leonardocosta.mg@diariosassociados.com.br informando que finalizou e link para o LinkedIn ou CV.

Vamos avaliar e dar o retorno.

**Boa sorte!**